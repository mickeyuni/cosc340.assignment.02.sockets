package main

import (
	"bufio"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"strings"
)

// Operation to checking if a file exists on the server.
// Requires a key (filename) and channel that accepts a bool.
type checkFile struct {
	key  string // filename
	resp chan bool
}

// Operation to get the base64 encoded string of a file on the server.
// Requires a key (filename) and channel that accepts a string.
type getFile struct {
	key  string // filename
	resp chan string
}

// Operation to get a list of all files current stored on the server
// in the current session. Requires a channel that accepts a slice strings.
type listAll struct {
	resp chan []string
}

// Operation to add a file to the file cache for the current server session.
// Requires a key (filename), base64 encoded string of the file data, and
// a channel that accepts a bool.
type addFile struct {
	key     string // filename
	b64file string // base64 encoded file
	resp    chan bool
}

// Bundle file operations for easier transport.
type FileOperations struct {
	checkChan chan *checkFile
	getChan   chan *getFile
	listChan  chan *listAll
	addChan   chan *addFile
}

func server(port *string) error {
	ln, err := net.Listen("tcp4", ":"+*port)
	if err != nil {
		return err
	}
	// Automatically close the listener when the function returns.
	defer ln.Close()

	// Make 3 channels, one for each operation we're supporting (download,
	// upload, list). Each channel accepts a pointer to that operations struct.
	fileOps := &FileOperations{
		checkChan: make(chan *checkFile),
		getChan:   make(chan *getFile),
		listChan:  make(chan *listAll),
		addChan:   make(chan *addFile),
	}

	go fileCache(fileOps)

	for {
		conn, err := ln.Accept()
		if err != nil {
			return err
		}
		Log.Print("Client connected, addr: ", conn.RemoteAddr().String())
		// Launch goroutine to handle connection. Allowing the server, freeing
		// the server to accept another connection.
		go handleConn(conn, fileOps)
	}
}

// Function to handle connections as they are received. Allows for concurrency when
// called as a goroutine. In the event of an error the error message is logged and the
// function is returned. When the function is returned the connection is automatically
// closed due to the defer statement. The next time the client attempts to read or
// write to the connection they will receive an io.EOF error, signalling the connection
// is closed.
func handleConn(conn net.Conn, fileOps *FileOperations) {
	// Close the connection automatically when the function returns
	defer conn.Close()
	ip := conn.RemoteAddr().String()
	defer Log.Printf("Client at %v disconnected.\n", ip)

	// A single interface for buffered reader and writer objects for sending and receiving
	// data over the network. The reader allows us to read incoming messages from the
	// network while the scanner is locked in an operation.
	rw := bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))
	// Scanner provides the primary input loop for receiving newline-delimited
	// data over the network. The scanner will continue to receive input until
	// it receives an io.EOF, or we break the loop.
	scanner := bufio.NewScanner(conn)

	for scanner.Scan() {
		args := strings.Split(scanner.Text(), " ")
		Log.Printf("Message received (%v): %v", ip, args)
		switch args[0] {
		case "LOGIN":
			if len(args) > 2 {
				// Client side validation should ensure this doesn't happen, but doesn't
				// hurt to be safer than sorry.
				Log.Printf("Error (%v): LOGIN has too many args. "+
					"Expected 1, received %v. Args %v", ip, len(args), args[1:])
				return
			} else {
				if _, err := rw.WriteString("LOGIN: OK\n"); err != nil {
					Log.Printf("Error (%v): %v", ip, err)
					return
				}
				rw.Flush()
			}
			Log.Printf("Success (%v): login %v", ip, args[1])
		case "LIST":
			listOp := &listAll{resp: make(chan []string)}
			// Send operation via channel
			fileOps.listChan <- listOp
			// Lock until response received and assign to variable
			files := <-listOp.resp
			for _, file := range files {
				// Send each file name followed by a line feed
				if _, err := rw.WriteString(file + "\n"); err != nil {
					Log.Printf("Error (%v): %v", ip, err)
					return
				}
			}
			if _, err := rw.WriteString("LIST: OK\n"); err != nil {
				Log.Printf("Error (%v): %v", ip, err)
				return
			}
			// Flush the buffer to send it all to the client
			rw.Flush()
			Log.Printf("Success (%v): sent %v", ip, files)
		case "UPLOAD":
			// Handle file names with spaces
			fName := strings.Join(args[1:], " ")
			if err := handleUpload(fName, rw, fileOps.checkChan, fileOps.addChan); err != nil {
				// io.EOF signals that the connection to the client has been closed
				if err == io.EOF {
					Log.Printf("Error (%v): EOF: Connection was terminated unexpectedly", ip)
					return
				}
				msg := "UPLOAD: FAIL\n"
				if _, err := rw.WriteString(msg); err != nil {
					return
				}
				rw.Flush()
				Log.Printf("Sent failure (%v): %v", ip, msg)
			} else {
				Log.Printf("Success (%v): received %v", ip, fName)
			}
		case "DOWNLOAD":
			// Handle file names with spaces
			fName := strings.Join(args[1:], " ")
			if err := handleDownload(fName, rw, fileOps.getChan); err != nil {
				if err == io.EOF {
					Log.Printf("Error (%v): EOF: Connection was terminated unexpectedly", ip)
					return
				}
				msg := "DOWNLOAD: FAIL\n"
				if _, err := rw.WriteString(msg); err != nil {
					return
				}
				rw.Flush()
				Log.Printf("Sent failure (%v): %v", ip, msg)
			} else {
				Log.Printf("Success (%v): sent %v", ip, fName)
			}
		case "EXIT":
			Log.Printf("Client (%v) has requested to close the connection", ip)
			return
		default:
			Log.Printf("Error (%v): Received invalid command, closing connection. args: (%v)", ip, args)
			return
		}
	}
}

// fileCache provides a basic in-memory record of the files uploaded within the
// current server session in addition to a cache of the files uploaded (base64
// encoded).
//
// A map is used to hold the filename and base64 encoded file strings already written
// to the file system. This serves as an in-memory record of files uploaded by clients,
// and as a cache so that files don't have to read and encoded again when a client
// requests to download. The map is not persisted beyond a single server session, which
// fulfills the requirement that "only files uploaded to the server should be available
// for users to download".
//
// I consciously made a few assumptions here:
// 		1. The files won't change on disk after they're written on the server side.
// 		2. We don't have to worry about the cache size on the server at the scale
// 		   we're working at, and therefore I haven't set a limit on the cache size,
// 		   nor configured any expiry of the base64 values stored in the cache. In typical
//		   circumstances (where it wouldn't be reasonable to make this assumption),
//		   we wouldn't simply allow the map to expand and consume memory endlessly.
//
// Maps are not thread safe so instead of risking a race condition we will use a
// goroutine to manage the map completely, and utilise channels to synchronise data
// access. In Golang this is more idiomatic than using mutexes.
//
// Reference: https://gobyexample.com/stateful-goroutines
// For information regarding channels: https://tour.golang.org/concurrency/2
func fileCache(fileOps *FileOperations) {
	files := make(map[string]string)
	for {
		select {
		case check := <-fileOps.checkChan:
			_, ok := files[check.key]
			check.resp <- ok
		case get := <-fileOps.getChan:
			get.resp <- files[get.key]
		case listAll := <-fileOps.listChan:
			keys := make([]string, len(files))
			i := 0
			for k := range files {
				keys[i] = k
				i++
			}
			listAll.resp <- keys
		case addFile := <-fileOps.addChan:
			files[addFile.key] = addFile.b64file
			addFile.resp <- true
		}
	}
}

// handleUpload negotiates the interaction required with the client in order to receive a base64 string
// encoded file, save it to the file system, and store the filename and string in the cache.
// In the event of an error, return up the stack.
func handleUpload(fName string, rw *bufio.ReadWriter, checkChan chan *checkFile, addChan chan *addFile) error {
	// Check the file cache if the file already exists
	checkOp := &checkFile{
		key:  fName,
		resp: make(chan bool)}
	checkChan <- checkOp
	if <-checkOp.resp {
		return errors.New("file already exists on server")
	}

	// Create file, or truncate if it already exists
	f, err := os.Create(fName)
	if err != nil {
		return err
	}
	// Close the file when the function returns
	defer f.Close()
	if _, err := rw.WriteString("UPLOAD: OK\n"); err != nil {
		return err
	}
	rw.Flush()

	resp, err := rw.ReadString('\n')
	if err != nil {
		return err
	}

	// Get the base64 string from the message received via the network
	b64str := strings.Trim(strings.Split(resp, " ")[1], "\n")
	// Decode the base64 string
	file, err := base64.StdEncoding.DecodeString(b64str)
	if err != nil {
		return err
	}
	// Write the contents to the file on disk
	if _, err := f.Write(file); err != nil {
		return err
	}
	// Ensure the file contents are committed to disk
	if err := f.Sync(); err != nil {
		return err
	}

	// Add the filename and content (base64 encoded string) to the file cache
	addOp := &addFile{
		key:     fName,
		b64file: b64str,
		resp:    make(chan bool)}
	addChan <- addOp
	// If the file cannot be added to the cache then return an error.
	if !<-addOp.resp {
		return errors.New("failed to save file")
	}
	if _, err = rw.WriteString("UPLOAD: OK\n"); err != nil {
		return err
	}
	rw.Flush()
	return nil
}

// handleDownload negotiates the interaction required with the client in order to receive a filename,
// retrieve the base64 encoded string for the file (if it exists in the server file cache), and sends
// it to the client. In the event of an error, return up the stack.
func handleDownload(fName string, rw *bufio.ReadWriter, getChan chan *getFile) error {
	// Retrieve the file from the cache via the getFile channel
	getOp := &getFile{
		key:  fName,
		resp: make(chan string),
	}
	getChan <- getOp
	b64str := <-getOp.resp

	// If the string received via the resp channel is empty then
	// return an error.
	if b64str == "" {
		return fmt.Errorf("file '%s' was not found in the cache", fName)
	}

	// Because the base64 encoded string of the file contents is cached,
	// and it is being assumed that the file will not change on disk,
	// there is no need to open the file to read it and create a new b64 string.
	if _, err := rw.WriteString("DOWNLOAD " + b64str + "\n"); err != nil {
		return err
	}
	if _, err := rw.WriteString("DOWNLOAD: OK\n"); err != nil {
		return err
	}
	rw.Flush()

	// The server has no retry mechanism, so it doesn't care if a DOWNLOAD: FAIL
	// is received after it sends the base64 encoded file contents. Therefor
	// we read the connection and discard the message.
	_, err := rw.ReadString('\n')
	if err != nil {
		return err
	}
	return nil
}
