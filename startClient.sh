#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Error: Invalid number of args. Expected 2, got: $#"
    echo "Usage: startServer.sh [hostname] port]"
    exit 1
fi

if ! [[ $2 =~ ^-?[0-9]+$ ]]; then
    echo "Error: $1 is not a valid port number."
    exit 1
fi

`dirname $0`/bin/jmichiel_a2 --mode client --hostname $1 --port $2 --logfile log/client.log