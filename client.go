package main

import (
	"bufio"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
	"strings"
)

func client(hostname *string, port *string) error {
	conn, err := net.Dial("tcp4", *hostname+":"+*port)
	if err != nil {
		return err
	}
	// Automatically close the connection when function returns
	defer conn.Close()
	Log.Print("Connected to server at:", conn.RemoteAddr().String())

	// Buffered reader to handle all user input
	input := bufio.NewReader(os.Stdin)
	// A single interface for buffered reader and writer objects for sending and receiving
	// data over the network.
	rw := bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))

	if err := handleLogin(rw, input); err != nil {
		return err
	}

	for {
		base, err := getOperation(input)
		if err != nil {
			return err
		}
		switch base {
		case "EXIT":
			if _, err := rw.WriteString(base + "\n"); err != nil {
				return err
			}
			rw.Flush()
			Log.Print("Message sent:", base)
			Log.Print("Connection closed by user")
			return nil
		case "LIST":
			if _, err := rw.WriteString(base + "\n"); err != nil {
				return err
			}
			Log.Print("Message sent: ", base)
			// Flush the buffer, forcing it to write to the underlying Writer
			// interface, which in this case is the TCP connection.
			rw.Flush()
			num := 0
			for {
				resp, err := rw.ReadString('\n')
				if err != nil {
					return err
				}
				if resp == "LIST: OK\n" {
					if num == 0 {
						fmt.Println("There are no files available for download.")
						Log.Print("Info: There are no files available for download")
					}
					break
				}
				Log.Print("Message received: ", strings.Trim(resp, "\n"))
				num++
				fmt.Print(resp)
			}
		case "UPLOAD":
			fmt.Println("Note: Both relative and absolute filepaths are allowed.")
			fmt.Print("Enter the file to upload: ")
			text, _ := input.ReadString('\n')
			var fPath string
			if fPath = strings.Trim(text, "\n"); fPath == "" {
				Log.Print("Info: User did not enter any input")
				fmt.Println("An error occurred: no input detected")
				continue
			}
			Log.Print("Info: Attempting to upload file: ", fPath)
			if err := handleClientSendFile(fPath, rw); err != nil {
				if err == io.EOF {
					return err
				}
				Log.Print("Error: ", err.Error())
				// Return to the menu if an error occurs. Don't re-prompt as the user
				// may wish to cancel the interaction.
				fmt.Println("An error occurred: ", err.Error())
				continue
			}
			Log.Print("Success: Successfully uploaded file: ", fPath)
			fmt.Println("Upload complete!")
		case "DOWNLOAD":
			fmt.Print("Enter the file to download: ")
			text, _ := input.ReadString('\n')
			var fName string
			if fName = strings.Trim(text, "\n"); fName == "" {
				Log.Print("Info: User did not enter any input")
				fmt.Println("An error occurred: no input detected")
				continue
			}
			Log.Print("Info: Attempting to download file: ", fName)
			if err := handleClientReceiveFile(fName, rw); err != nil {
				if err == io.EOF {
					return err
				}
				Log.Print("Error: ", err.Error())
				// Return to the menu if an error occurs. Don't re-prompt as the user
				// may wish to cancel the interaction.
				fmt.Println("An error occurred:", err.Error())
				continue
			}
			Log.Print("Success: Successfully downloaded file: ", fName)
			fmt.Println("Download complete!")
		default:
		}
	}
}

// handleLogin will prompt the user for a username, validate the input, and
// negoiate the request with the server. If the user input is invalid, the
// user will be prompted to try again.
func handleLogin(rw *bufio.ReadWriter, input *bufio.Reader) error {
	// Validate the username provided by the user. If the input is valid the
	// returned error is nil, otherwise it will contain the validation error.
	validateUsrName := func(usrName string) error {
		args := strings.Split(strings.Trim(usrName, "\n"), " ")
		if len(args) != 1 {
			return errors.New("username invalid - cannot contain spaces")
		} else if args[0] == "" {
			return errors.New("input invalid - you must specify a username")
		} else if strings.Contains(args[0], "\\n") || strings.Contains(strings.ToLower(args[0]), "\\x0a") {
			// ReadStrings will escape "\n" and "\x0(a/A)" when manually entered by the user.
			// Instead of sending obviously invalid input to the server, we will instead throw and error.
			return errors.New("username invalid - cannot contain line breaks")
		}
		return nil
	}

	var cmd string
	// Loop until the user enters a valid username
	for {
		fmt.Print("Please enter your username: ")
		text, _ := input.ReadString('\n')
		if err := validateUsrName(text); err != nil {
			Log.Print("Error: Username validation error: ", err.Error())
			fmt.Println("Error:", err.Error()+". Please try again\n")
		} else {
			cmd = fmt.Sprintf("LOGIN %s", text)
			break
		}
	}

	if _, err := rw.WriteString(cmd); err != nil {
		return err
	}
	rw.Flush()
	Log.Print("Message sent: ", strings.Trim(cmd, "\n"))

	resp, err := rw.ReadString('\n')
	if err != nil {
		return err
	}
	resp = strings.Trim(resp, "\n")
	if resp != "LOGIN: OK" {
		return fmt.Errorf("unexpected response from server: %s", resp)
	}
	Log.Print("Received response: ", resp)
	fmt.Println(resp)
	return nil
}

// getOperation will print the menu, prompt the user, and return
// the command associated with the users selection. If the user
// an invalid selection then they are notified and re-prompted.
func getOperation(rw *bufio.Reader) (string, error) {
	for {
		fmt.Println()
		fmt.Println("Select an option (1-4):")
		fmt.Println()
		fmt.Println("1. List all files available for download.")
		fmt.Println("2. Download a file.")
		fmt.Println("3. Upload a file.")
		fmt.Println("4. Exit.")
		fmt.Println()
		fmt.Print("Enter selection: ")
		input, err := rw.ReadString('\n')
		if err != nil {
			return "", err
		}

		switch strings.Trim(input, "\n") {
		case "1":
			Log.Println("Selection: LIST")
			return "LIST", nil
		case "2":
			Log.Println("Selection: DOWNLOAD")
			return "DOWNLOAD", nil
		case "3":
			Log.Println("Selection: UPLOAD")
			return "UPLOAD", nil
		case "4":
			Log.Println("Selection: EXIT")
			return "EXIT", nil
		default:
			Log.Print("Error: User entered invalid number: ", strings.Trim(input, "\n"))
			fmt.Println("Invalid selection, try again.")
		}
	}
}

// handleClientSendFile negotiates the interaction required with the server in order
// to upload a file from the local file system as a base64 encoded string.
// In the event of an error, return up the stack.
func handleClientSendFile(fPath string, rw *bufio.ReadWriter) error {
	f, err := os.Open(fPath)
	if err != nil {
		return err
	}
	defer f.Close()

	// Reference: https://stackoverflow.com/a/34527720/6050866
	// Isolate just the filename, ignoring the path
	fName := filepath.Base(fPath)
	msg := "UPLOAD " + fName + "\n"
	if _, err = rw.WriteString(msg); err != nil {
		return err
	}
	rw.Flush()
	Log.Print("Message sent: ", msg)

	resp, err := rw.ReadString('\n')
	if err != nil {
		return err
	}
	if resp != "UPLOAD: OK\n" {
		return errors.New("the server rejected the file, it may already exist")
	}

	// Reference: https://www.socketloop.com/tutorials/golang-encode-image-to-base64-example
	// Create a byte buffer to read file into.
	fInfo, _ := f.Stat()
	fSize := fInfo.Size()
	buf := make([]byte, fSize)
	fReader := bufio.NewReader(f)
	fReader.Read(buf)
	// Convert to base64 string.
	b64str := base64.StdEncoding.EncodeToString(buf)
	// Log.Println(b64str)

	if _, err = rw.WriteString("UPLOAD " + b64str + "\n"); err != nil {
		return err
	}
	if _, err = rw.WriteString("UPLOAD: OK\n"); err != nil {
		return err
	}
	rw.Flush()
	Log.Print("Message sent: base64 encoded string, value ommitted")

	resp, err = rw.ReadString('\n')
	if err != nil {
		return err
	}
	if resp != "UPLOAD: OK\n" {
		return errors.New("the upload to the server failed")
	}

	return nil
}

// handleClientReceiveFile negotiates the interaction required with the server in order
// to download a file from the server in the form of a base64 encoded string. Convert the string
// and save it to the local file system. In the event of an error, return up the stack.
func handleClientReceiveFile(fName string, rw *bufio.ReadWriter) error {
	msg := "DOWNLOAD " + fName + "\n"
	if _, err := rw.WriteString(msg); err != nil {
		return err
	}
	rw.Flush()
	Log.Print("Message sent: ", msg)

	resp, err := rw.ReadString('\n')
	if err != nil {
		return err
	}
	if resp == "DOWNLOAD: FAIL\n" {
		return errors.New("download failed, file may not exist on the server")
	}
	if _, err = rw.ReadString('\n'); err != nil {
		return err
	}

	// Create file, or truncate if it already exists
	f, err := os.Create(fName)
	if err != nil {
		return err
	}
	// Close the file when the function returns
	defer f.Close()

	// Get the base64 string from the message received via the network
	b64str := strings.Trim(strings.Split(resp, " ")[1], "\n")
	// Decode the base64 string
	file, err := base64.StdEncoding.DecodeString(b64str)
	if err != nil {
		return err
	}
	// Write the contents to the file on disk
	if _, err := f.Write(file); err != nil {
		return err
	}
	// Ensure the contents of the file have been committed to disk
	if err := f.Sync(); err != nil {
		return err
	}
	Log.Print("Successfully wrote file to disk:", fName)

	if _, err = rw.WriteString("DOWNLOAD: OK\n"); err != nil {
		return err
	}
	rw.Flush()
	Log.Print("Message sent: DOWNLOAD: OK")

	return nil
}
