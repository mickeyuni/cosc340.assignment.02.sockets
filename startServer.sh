#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Error: Invalid number of args. Expected 1, got: $#"
    echo "Usage: startServer.sh [port]"
    exit 1
fi

if ! [[ $1 =~ ^-?[0-9]+$ ]]; then
    echo "Error: $1 is not a valid port number."
    exit 1
fi

`dirname $0`/bin/jmichiel_a2 --mode server --port $1