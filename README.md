# COSC340 Assignment 2

## Overview

Name: Josh Michielsen  
Student No.: 220178453

My submission for this assignment is written in golang. The solution is split into three files:
- `main.go`: Main entrypoint for the application. Configure the logger and launches the correct mode as specified by the user.
- `server.go`: Handles all server logic when the user specifies `--mode server`. Supports multiple client connections by delegating the handler for each connection to a goroutine, as well as managing the in-memory file record via channels in order to synchronise access.
- `client.go`: Handles all client based interaction. Includes client side input validation, and detailed logging that can be configured via the `--logfile` flag.

### Usage

To build and run the application:

- It is recommended that you copy the folder these files reside in to your `$GOPATH/src` directory (e.g. `$GOPATH/src/jmichiel_a2`). If `$GOPATH` isn't set within the current session you can find it via the `go env GOPATH` command.
- Change directory to the project directory and run `go build *.go -o ./bin/jmichiel_a2`.
- The binary accepts the following command-line arguments:
  - `--mode`: Specify if you wish to launch in `server` or `client` mode.
  - `--port`: The port number to listen or connect to, defaults to 9000.
  - `--hostname`: The hostname to listen via or connect to, defaults to `localhost`.
  - `--logfile`: Specify the relative or absolute path to output log information. By default Golang outputs to Stderr. (e.g. `--logfile /var/log/jmichiel-a2-client.log`).
- It is recommended you choose to log to a file when running the client as the output can be quite noisy.

Example:  
`/path/to/binary --mode server --port 9001 --logfile /var/log/jmichiel-a2-server.log`

### Shell scripts

Shell scripts have been provided as per the requirement. When running the scripts it is assumed that:

1. The code has been compiled as per step 1 & 2 in the [Usage](#Usage) information
2. The binary is located at `bin/jmichiel_a2`.

`startServer.sh` accepts one parameter - a port number, e.g.:
`startServer.sh 9000`

`startClient.sh` accepts a hostname followed by a port number, e.g.:
`startClient.sh localhost 9000`

The shell script is configured so that the client will automatically log to `$cwd/log/client.log` to avoid noisy output.

## Notes:

- Golangs `encoding/base64` library is defined in RFC 4648 and used in MIME (RFC 2045). [src](https://golang.org/pkg/encoding/base64/)
- It's idiomatic in go to use short variable names, especially for common elements such as files, readers, writers, etc. I'm not generally a big fan of this, but I think it's important to take queues from a languages community. Hopefully it's still quite readable!
- Please see `server.go:#187` for details of file cache implementation. Hopefully my assumptions are reasonable!