package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// TODO:
//      Encode file and send as b64 string
//

// Create a global logger
// Loggers are thread safe in Go
var Log *log.Logger

func main() {
	mode := flag.String("mode", "", "Required. Application mode to launch. Valid input: 'server' or 'client'.")
	hostname := flag.String("hostname", "localhost", "Hostname of the server.")
	port := flag.String("port", "9000", "Port to listen or connect.")
	logfile := flag.String("logfile", "", "Specify a location to store the log file. (default \"stderr\")")
	flag.Parse()

	retcode := 0
	// Deferring the os.Exit() will allow us to honour other defer calls.
	// https://stackoverflow.com/q/27629380/6050866
	defer func() {
		os.Exit(retcode)
	}()

	f, err := configureLogger(*logfile)
	if err != nil {
		log.Print(err)
		retcode = 1
		return
	}
	if f != nil {
		defer f.Close()
	}

	switch strings.ToLower(*mode) {
	case "server":
		Log.Print("Launching application in server mode")
		if err := server(port); err != nil {
			retcode = 1
			Log.Print("Error: ", err)
			return
		}
	case "client":
		Log.Print("Launching application in client mode")
		if err := client(hostname, port); err != nil {
			retcode = 1
			if err == io.EOF {
				Log.Print("Error: EOF: server terminated the connection")
				fmt.Println("Server terminated the connection unexpectedly")
				return
			}
			Log.Print("Error: ", err)
			fmt.Println("An error occured:", err)
			return
		}
		fmt.Print("Connection has been closed")
	default:
		Log.Print("Error: invalid value for flag --mode")
		flag.Usage()
		retcode = 1
	}
	return
}

// configureLogger will create or open log file and configure global
// logger to output to a file with the specified filename. If returns
// the file pointer and an error value (if any).
func configureLogger(fPath string) (*os.File, error) {
	// If no logfile specified, log to Stderr (default).
	if fPath == "" {
		Log = log.New(os.Stderr, "", log.Lshortfile|log.LstdFlags)
		return nil, nil
	}

	dir := filepath.Dir(fPath)

	// Create directory if it doesn't already exist
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err := os.MkdirAll(dir, 0755)
		if err != nil {
			return nil, err
		}
	}
	// Open the file and append. Creates the file if it doesn't already exist.
	f, err := os.OpenFile(fPath, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return nil, err
	}
	Log = log.New(f, "", log.Lshortfile|log.LstdFlags)
	return f, nil
}

// Global error handler, used during development
func checkError(err error) {
	if err != nil {
		log.Print("Error:", err.Error())
		panic(err)
	}
}
